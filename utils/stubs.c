#include "stubs.h"

TNumber __stub__(void) {
	return 0;
}

TValue __stub1__(TValue a) {
	return 0;
}

TValue __stub2__(TValue a, TValue b) {
	return 0;
}

TValue __stub3__(TValue a, TValue b, TValue c) {
	return 0;
}

TValue __stub4__(TValue a, TValue b, TValue c, TValue d) {
	return 0;
}
