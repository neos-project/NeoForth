#pragma once

#include "../defs.h"

typedef void(*TOnFreeFunc)(TAddress);



void MemoryInit(bool hard_free);
TAddress MemoryNew(UNumber size, TOnFreeFunc func);
UNumber MemorySize(TAddress ptr);

void MemoryUse(TAddress addr);
void MemoryUseless(TAddress addr);

void MemoryUseWith(TValueArgs ptrs);
void MemoryUselessWith(TValueArgs ptrs);

void MemoryDelete(TAddress ptr);
void MemoryGC(void);
void MemoryClean(void);



#ifdef test
void memoryPrint(void);

void onFree(TAddress data) {
	printf("Free %hu\n", data);
}

void mem_test() {
	MemoryInit(false);
	for (int i=0; i<20; i++) {
		TAddress mem = MemoryNew(128, onFree);
		MemoryUseless(mem);
		memoryPrint();
		// printf("%d\t%d\n", mem, MemorySize(mem));
	}
	return;
}
#endif
