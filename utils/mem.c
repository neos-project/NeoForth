#include "mem.h"
#include "atexit.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

// Constants
enum {
	MAX_PART = 16,
	MAX_GC_FREE = 0 /* 0 = ALL */
};

// Types
typedef TValue TMemID, *PMemID;

typedef struct MemPart {
	void *ptr;
	UNumber size;
	UNumber use_count;
	TOnFreeFunc on_free;
} TMemPart, *PMemPart;

// Global vars
static PMemPart Heap[MAX_PART];
static UNumber _last;
static bool hard_free_policy;


//
// Private methods
//
static PMemPart memoryPartNew(void) {
	TNumber size = sizeof(TMemPart);
	PMemPart result = (PMemPart) calloc(size, 1);
	if (!result)
		return null;
	return result;
}

static void memoryPartDelete(PMemPart part) {
	if (!part)
		return;
	void *ptr = part->ptr;
	if (part->size && ptr) {
		TOnFreeFunc f = part->on_free;
		part->size = 0;
		if (f) {
			f(newAddress(sizeof(TMemID) + ptr));
		}
		free(ptr);
	}
	memset(part, 0, sizeof(TMemPart));
	free(part);
}

static TMemID findFree() {
	bool first_try = true;
	UNumber cur = _last;
	if (cur >= MAX_PART)
		cur = 0;
	while (Heap[cur]) {
		if (++cur > MAX_PART)
			cur = 0;
		if (cur == _last) {
			if (first_try--) {
				MemoryGC();
				continue;
			}
			return 0;
		}
	}
	return fromUNumber(cur);
}

static void memoryPush(UNumber free_id, PMemPart part) {
	Heap[free_id] = part;
	_last = free_id + 1;
}

static void memoryPop(UNumber id) {
	if (id + 1 == _last) {
		_last--;
	}
	Heap[id] = 0;
}

static TMemID getMemId(TAddress ptr) {
	UNumber *_id = (UNumber *)(ptr - sizeof(TMemID));
	UNumber id = *_id;
	return (id < MAX_PART)? fromUNumber(id): null;
}

static void memoryDeleteById(UNumber id) {
	PMemPart part = Heap[id];
	memoryPartDelete(part);
	memoryPop(id);
}

static bool memoryUseArg(TValue arg) {
	if (!arg || !isAddress(arg)) {
		return false;
	}
	TAddress addr = toAddress(arg);
	TMemID id_ = getMemId(addr);
	UNumber id = toUNumber(id_);
	if (id_ && Heap[id]) {
		Heap[id]->use_count++;
	}
	return false;
}

static bool memoryUselessArg(TValue arg) {
	if (!arg || !isAddress(arg)) {
		return false;
	}
	TAddress addr = toAddress(arg);
	TMemID id_ = getMemId(addr);
	UNumber id = toUNumber(id_);
	if (id_ && Heap[id]) {
		if (Heap[id]->use_count)
			Heap[id]->use_count--;
		if (hard_free_policy)
			memoryDeleteById(id);
	}
	return false;
}


//
// Public methods
//
void MemoryInit(bool hard_free) {
	hard_free_policy = hard_free;
	memset(Heap, 0, sizeof(PMemPart) * MAX_PART);
	_last = 0;
	AtExitDo(MemoryClean);
}

TAddress MemoryNew(UNumber size, TOnFreeFunc func) {
	UNumber data_size = sizeof(TMemID) + size;

	TMemID free_id_ = findFree();
	if (!free_id_)
		return null;
	UNumber free_id = toUNumber(free_id_);

	UNumber *data = calloc(data_size, 1);
	if (!data)
		return null;

	PMemPart part = memoryPartNew();
	if (!part) {
		free(data);
		return null;
	}

	*data = free_id;
	part->ptr = data;
	part->size = size;
	part->on_free = func;
	part->use_count = 1;
	memoryPush(free_id, part);
	return (TAddress)data + sizeof(TMemID);
}

UNumber MemorySize(TAddress ptr) {
	UNumber result = 0;
	if (!ptr)
		return null;
	TMemID id_ = getMemId(ptr);
	UNumber id = toUNumber(id_);
	if (id_) {
		result = Heap[id]->size;
	}
	return result;
}

void MemoryUse(TAddress addr) {
	memoryUseArg(fromAddress(addr));
}

void MemoryUseless(TAddress addr) {
	memoryUselessArg(fromAddress(addr));
}

void MemoryUseWith(TValueArgs args) {
	forEachArgs(args, memoryUseArg);
}

void MemoryUselessWith(TValueArgs args) {
	forEachArgs(args, memoryUselessArg);
}

void MemoryDelete(TAddress ptr) {
	TMemID id = getMemId(ptr);
	if (id) {
		memoryDeleteById(toUNumber(id));
	}
}

void MemoryGC(void) {
	UNumber free = MAX_GC_FREE;
	for (UNumber id = 0; id < _last; id++) {
		if (Heap[id] && Heap[id]->use_count == 0) {
			memoryDeleteById(id);
			if (MAX_GC_FREE && --free == 0) {
				break;
			}
		}
	}
}

void MemoryClean(void) {
	for (UNumber id = 0; id < MAX_PART; id++) {
		PMemPart part = Heap[id];
		if (part)
			memoryPartDelete(Heap[id]);
	}
	memset(Heap, 0, sizeof(PMemPart) * MAX_PART);
}


//
// Extra
//
void memoryPrint(void) {
	puts("");
	for (UNumber id=0; id<MAX_PART; id++) {
		printf("%-8x", Heap[id]? Heap[id]->ptr: 0);
	}
	puts("");
}
