#include "atexit.h"
#include <stdlib.h>
#include <assert.h>

// Constants
enum {
	MAX_FUNC = 32
};

// Global vars
static TAtExitFunc at_exit[MAX_FUNC];
static int at_exit_count = 0;


//
// Private methods
//
static void atExitCall(void) {
	for (int i=0; i<at_exit_count; i++) {
		TAtExitFunc func = at_exit[i];
		func();
	}
}


//
// Public methods
//
void AtExitDo(TAtExitFunc func) {
	if (!at_exit_count) {
		atexit(atExitCall);
	}
	at_exit[at_exit_count++] = func;
	assert(at_exit_count <= MAX_FUNC);
}
