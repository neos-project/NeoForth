#include <stdlib.h>
#include <string.h>
#include "parser.h"
#include "mem.h"


//
// Public methods
//
PParserState Parser(char *source, TNumber size) {
	PParserState state =
	    (PParserState) MemoryNew(
	        sizeof(TParserState), null
	    );

	if (state) {
		state->base = source;
		state->size = size;
	}
	return state;
}

bool EndOfBuf(PParserState state) {
	return state->offset >= state->size;
}

char CurrentChar(PParserState state) {
	return state->base[state->offset];
}

char PervChar(PParserState state) {
	return (state->offset > 0)? state->base[--state->offset]: 0;
}

char NextChar(PParserState state) {
	return !EndOfBuf(state)? state->base[state->offset++]: 0;
}

bool SkipDelims(PParserState state, TParserFunc isDelim, int param) {
	if (EndOfBuf(state))
		return false;
	while (isDelim(CurrentChar(state), param)) NextChar(state);
	return true;
}

bool SkipNotDelims(PParserState state, TParserFunc isDelim, int param) {
	if (EndOfBuf(state))
		return false;
	while (!isDelim(CurrentChar(state), param)) NextChar(state);
	return true;
}


//
// Base parse methods
//
static bool is_space(char ch, int param) {
	return ch <= ' ';
}

static bool is_char(char ch, int param) {
	return ch == (char)param;
}

static char *fillParseBuf(PParserState state, char *offset, TNumber size) {
	if (size > MAX_PARSE_BUF) return 0;
	memcpy((void *)state->parse_buf, (void *)offset, size);
	state->parse_buf[size] = 0;
	state->parse_size = size;
	NextChar(state);
	return state->parse_buf;
}

char *ToChar(PParserState state, char ch) {
	int start = state->offset;
	if (!SkipNotDelims(state, is_char, ch))
		return null;
	int end = state->offset;
	int size = end - start;
	return fillParseBuf(state, state->base + start, size);
}

char *NextWord(PParserState state) {
	if (!SkipDelims(state, is_space, 0))
		return null;
	int start = state->offset;
	if (!SkipNotDelims(state, is_space, 0))
		return null;
	int end = state->offset;
	int size = end - start;
	return fillParseBuf(state, state->base + start, size);
}
