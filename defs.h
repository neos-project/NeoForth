#pragma once

#include <stdint.h>
#include <stdbool.h>

typedef uintptr_t TAddress;
typedef intptr_t TNumber;
typedef uintptr_t UNumber;
typedef TNumber TValue;

// voc.h
typedef TValue TWordId;

#define null 0
#define null0 1

typedef TValue *TValueArgs;
#define args(values...)(TValue[]){values, 0}



// TAddress/TValue
TAddress newAddress(void *ptr);
bool isAddress(TValue value);
TAddress toAddress(TValue value);
TValue fromAddress(TAddress addr);

// Number/TValue
bool isNumber(TValue value);
TNumber toTNumber(TValue value);
UNumber toUNumber(TValue value);
TValue fromTNumber(TNumber num);
TValue fromUNumber(UNumber num);

// TValueArgs
void forEachArgs(TValueArgs args, bool (*func)(TValue arg));

// Extra
void printBits(size_t const size, void const* const ptr);



#ifdef test
#include <assert.h>
#include <stdio.h>

void test_types() {
	TNumber n = -5;
	TValue nv = fromTNumber(n);
	TNumber n2 = toTNumber(nv);
	UNumber n3 = toUNumber(nv);
	printBits(sizeof(n), &n);
	printBits(sizeof(nv), &nv);
	printBits(sizeof(n2), &n2);
	printBits(sizeof(n3), &n3);
	assert(n == n2);

	TAddress n_addr = newAddress(&n);
	TValue n_addrv = fromAddress(n_addr);
	TAddress n_addr2 = toAddress(n_addrv);
	printBits(sizeof(n_addr), &n_addr);
	printBits(sizeof(n_addrv), &n_addrv);
	printBits(sizeof(n_addr2), &n_addr2);
	assert(n_addr == n_addr2);

	puts("");
}
#endif
