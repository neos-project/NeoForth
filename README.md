# NeoForth

[![License: GPL v3](https://img.shields.io/badge/License-GPLv3-blue.svg)](https://www.gnu.org/licenses/gpl-3.0)

[TOC]

## What is?
- *todo*

## Installation
- *todo*

## Getting started
- *todo*

## Examples
- *todo*

## Authors and acknowledgment
- [Alex Krol](https://gitlab.com/Krol-X) ([krolmail@list.ru](mailto:krolmail@list.ru)) – author
- [NDRAEY](https://gitlab.com/AndreyTheHacker) – contributor
