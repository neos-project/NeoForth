#pragma once

#include "utils/mem.h"
#include "stdbool.h"

enum {
	MAX_PARSE_BUF = 512
};

typedef struct ParserState {
	char *base;
	TNumber offset;
	TNumber size;
	char parse_buf[MAX_PARSE_BUF];
	TNumber parse_size;
} TParserState, *PParserState;

typedef bool(*TParserFunc)(char ch, int param);



PParserState Parser(char *source, TNumber size);

bool EndOfBuf(PParserState state);
char CurrentChar(PParserState state);
char PervChar(PParserState state);
char NextChar(PParserState state);
bool SkipDelims(PParserState state, TParserFunc isDelim, int param);
bool SkipNotDelims(PParserState state, TParserFunc isDelim, int param);

char *ToChar(PParserState state, char ch);
char *NextWord(PParserState state);



#ifdef test
#include <stdio.h>
#include <string.h>

void fin(TAddress addr) {
	printf("Free");
}

PParserState _Parser(char *source, TNumber size) {
	PParserState state =
	    (PParserState) MemoryNew(
	        sizeof(TParserState), fin
	    );
	state->base = source;
	state->size = size;
	return state;
}

void parser_test() {
	char test_str[] = " Test string  yes' ";
	PParserState parser = _Parser(test_str, strlen(test_str));

	puts(NextWord(parser));
	puts(NextWord(parser));
	puts(ToChar(parser, '\''));
	MemoryUseless(newAddress(parser));
	printf("END");
}

#endif
