#pragma once

#include "parser.h"
#include "voc/voc.h"
#include "stack.h"

typedef enum {
	ERR_EXECSTATE = -4,
	ERR_GET_WORD = -3,
	ERR_WORD = -2,
	ERR_INIT = -1,
	OK = 0,
	RUNNING = 1,
	BYE = 2
} TInterpretState;

typedef struct InterpretSession {
	TInterpretState state;
	PParserState parser;
	PVocContext context; // todo: multivoc context
	PStackState stack;
} TInterpretSession, *PInterpretSession;



PInterpretSession Interpretator(char *src, UNumber stack_size);
TNumber Interpret(PInterpretSession session);
