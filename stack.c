#include "stack.h"
#include "defs.h"
#include "utils/mem.h"
#include <assert.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>

//
// Types
//
struct StackState {
	TNumber *data;
	TNumber top;
};


//
// Constructor and destructor
//
static void _Stack(TAddress addr) {
	PStackState stack = (PStackState)addr;
	MemoryUseless(newAddress(stack->data));
}

PStackState Stack(TNumber maxSize) {
	PStackState state =
	    (PStackState) MemoryNew(
	        sizeof(TStackState), _Stack
	    );
	if (!state) {
		return null;
	}

	state->data = (TNumber *)
	              MemoryNew(maxSize*sizeof(TNumber), null);
	if (!state->data) {
		MemoryDelete(newAddress(state));
		return null;
	}
	return state;
}


//
// Public methods
//
TNumber StackSize(PStackState state) {
	return MemorySize(newAddress(state->data)) / sizeof(TNumber);
}

TNumber StackCount(PStackState state) {
	return state->top;
}

void StackClear(PStackState state) {
	state->top = 0;
}

bool StackPush(PStackState state, TNumber value) {
	TNumber size = StackSize(state);
	if (state->top >= size) {
		state->top = size;
		return false;
	}
	state->data[state->top++] = value;
	return true;
}

TNumber StackPop(PStackState state) {
	if (state && state->top > 0) {
		return state->data[--state->top];
	}
	return 0;
}

void StackPick(PStackState state, TNumber n) {
	TNumber count = StackCount(state);
	if (n < count) {
		StackPush(state, state->data[count - 1 - n]);
	}
}

void StackRoll(PStackState state, TNumber n) {
	TNumber count = StackCount(state);
	TNumber idx = count - 1 - n;
	TNumber item = state->data[idx];
	memcpy(&state->data[idx], &state->data[idx + 1], count - 1 - idx);
	state->data[count - n] = item;
}

void StackPrint(PStackState state, TNumber min) {
	TNumber count = StackCount(state);
	if (min && count > min) {
		printf("... ");
	} else {
		min = count;
	}
	for (TNumber i = 0; i < min; i++) {
		printf("%d ", state->data[count - 1 - i]);
	}
}

void StackPrintDirect(PStackState state, TNumber min) {
	TNumber count = StackCount(state);

	if (!min) {
		min = count;
	}
	for (TNumber i = 0; i < min; i++) {
		printf("%d ", state->data[i]);
	}
	if (count > min) {
		printf("...");
	}
}
