#pragma once

#include "defs.h"

struct StackState;
typedef struct StackState TStackState, *PStackState;



PStackState Stack(TNumber maxSize);

TNumber StackSize(PStackState state);
TNumber StackCount(PStackState state);
void StackClear(PStackState state);
bool StackPush(PStackState state, TNumber value);
TNumber StackPop(PStackState state);
void StackPick(PStackState state, TNumber n);
void StackRoll(PStackState state, TNumber n);

void StackPrint(PStackState state, TNumber min);
void StackPrintDirect(PStackState state, TNumber min);
