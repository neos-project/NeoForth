#include <string.h>
#include "voc.h"
#include "../buildin.h"
#include "../utils/stubs.h"


//
// Vocabulary methods
//
static TNumber __count(PVocContext ctx) {
	return BUILDIN_COUNT;
}

static TWordId __find(PVocContext ctx, char *name) {
	for (TNumber i = 0; i < BUILDIN_COUNT; i++) {
		if (strcmp(name, BUILDIN_WORDS[i]->name) == 0) {
			return fromTNumber(i);
		}
	}
	return null;
}

static TWordId __next(PVocContext ctx, TWordId id) {
	TNumber _id = toTNumber(id);
	if (_id < BUILDIN_COUNT)
		return _id;
	else
		return null;
}

// todo: How can be better?
static PWord __getWord(PVocContext ctx, TWordId id) {
	TNumber _id = toTNumber(id);
	if (_id < BUILDIN_COUNT)
		return BUILDIN_WORDS[_id];
	else
		return null;
}


//
// Tables and contructor
//
static TVocMethods methods = {
	.Count = __count,
	.Find = __find,
	.Next = __next,
	.GetWord = __getWord,
	.SetWord = __stub2__,
	.NewWord = __stub2__
};

static TVocContext context = {
	.methods = &methods,
	.data = null
};

PVocContext SystemVoc(void) {
	return &context;
}
