#pragma once

struct Module;
typedef struct Module TModule, *PModule;

typedef struct Word {
	char *name;
	PModule module;
	void *data;
} TWord, *PWord;
