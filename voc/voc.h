#pragma once

#include "../defs.h"
#include "word.h"

struct VocMethods;
typedef struct VocContext {
	struct VocMethods *methods;
	void *data;
} TVocContext, *PVocContext;

typedef struct VocMethods {
	TNumber (*Count)(PVocContext ctx);
	TWordId (*Find)(PVocContext ctx, char *name);
	TWordId (*Next)(PVocContext ctx, TWordId id);
	PWord (*GetWord)(PVocContext ctx, TWordId id);
	bool (*SetWord)(PVocContext ctx, TWordId id, PWord word);
	TWordId (*NewWord)(PVocContext ctx, char *name);
} TVocMethods, *PVocMethods;



PVocContext SystemVoc(void);
PVocContext NumberVoc(void);
