#include "voc.h"
#include "../module/mod.h"
#include "../utils/stubs.h"
#include <string.h>
#include <stdlib.h>


//
// Vocabulary methods
//
static TNumber __count(PVocContext ctx) {
	return 0;
}

static TWordId __find(PVocContext ctx, char *name) {
	char *end;
	TWordId n = strtol(name, &end, 10);
	return (end == name)? null: n;
}

static TWordId __next(PVocContext ctx, TWordId id) {
	return null;
}

static PWord __getWord(PVocContext ctx, TWordId id) {
	PWord result = (PWord) MemoryNew(sizeof(TWord), null);
	if (result) {
		result->module = &_NUMBER_MODULE;
		result->data = (void *) id;
	}
	return result;
}


//
// Tables and contructor
//
static TVocMethods methods = {
	.Count = __count,
	.Find = __find,
	.Next = __next,
	.GetWord = __getWord,
	.SetWord = __stub2__,
	.NewWord = __stub2__
};

static TVocContext context = {
	.methods = &methods,
	.data = null
};

PVocContext NumberVoc(void) {
	return &context;
}
