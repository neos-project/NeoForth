#include "interpreter.h"
#include <string.h>
#include "module/mod.h"
#include "stack.h"

// Constants
enum {
	MIN_STACK_SIZE = 8
};


//
// Constructor and destructor
//
static void _Interpretator(TAddress addr) {
	if (addr) {
		PInterpretSession session = (PInterpretSession) addr;
		MemoryUseless((TAddress)session->parser);
		MemoryUseless((TAddress)session->stack);
	}
}

PInterpretSession Interpretator(char *src, UNumber stack_size) {
	PInterpretSession session =
	    (PInterpretSession) MemoryNew(
	        sizeof(TInterpretSession),
	        _Interpretator
	    );

	if (session) {
		session->state = ERR_INIT;

		PVocContext context = SystemVoc();
		if (!context)
			return session;

		PParserState parser = Parser(src, strlen(src));
		if (!parser) {
			return session;
		}

		if (stack_size <= 0)
			stack_size = MIN_STACK_SIZE;
		PStackState stack = Stack(stack_size);
		if (!stack) {
			MemoryDelete((TAddress)parser);
			return session;
		}

		session->context = context;
		session->parser = parser;
		session->stack = stack;
		session->state = OK;
	}
	return session;
}


//
// Public methods
//
TNumber Interpret(PInterpretSession session) {
	if (!session) {
		return ERR_INIT;
	}
	PVocMethods action = session->context->methods;
	PVocContext num_ctx = NumberVoc(); // todo: add to multivoc context

	session->state = RUNNING;
	while (session->state == RUNNING) {
		action = session->context->methods;
		char *text_word = NextWord(session->parser);
		if (text_word == null) {
			session->state = OK;
			break;
		}
		TWordId wid = action->Find(session->context, text_word);

		// Try to recognize number (without multivoc context)
		if (!wid) {
			action = num_ctx->methods;
			wid = action->Find(num_ctx, text_word);
		}

		if (!wid) {
			session->state = ERR_WORD;
			break;
		}
		PWord word = action->GetWord(session->context, wid);
		if (!word) {
			session->state = ERR_GET_WORD;
			break;
		}
		PModule module = word->module;
		PExecuteState state = module->Init(session, word);
		if (!state) {
			session->state = ERR_EXECSTATE;
			break;
		}
		module->Execute(state);
		MemoryUseless(newAddress(state));
		if (action == num_ctx->methods) {
			MemoryUseless((TAddress)word);
		}
	}
	StackPrint(session->stack, 5);
	return session->state;
}
