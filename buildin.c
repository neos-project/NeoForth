#include <stdio.h>
#include "buildin.h"


//
// Buildin methods
//
static TWord w_hello = {
	.name = "hello",
	.module = &_SYSTEM_MODULE,
	.data = __hello
};
void __hello(PExecuteState state) {
	printf("Hello :)\n");
}



static TWord w_bye = {
	.name = "bye",
	.module = &_SYSTEM_MODULE,
	.data = __bye
};
void __bye(PExecuteState state) {
	state->session->state = BYE;
}



static TWord w_false = {
	.name = "false",
	.module = &_SYSTEM_MODULE,
	.data = __false
};
void __false(PExecuteState state) {
	StackPush(state->session->stack, 0);
}



static TWord w_true = {
	.name = "true",
	.module = &_SYSTEM_MODULE,
	.data = __true
};
void __true(PExecuteState state) {
	StackPush(state->session->stack, -1);
}



static TWord w_add = {
	.name = "+",
	.module = &_SYSTEM_MODULE,
	.data = __add
};
void __add(PExecuteState state) {
	PStackState stack = state->session->stack;
	StackPush(stack, StackPop(stack) + StackPop(stack));
}



static TWord w_mul = {
	.name = "*",
	.module = &_SYSTEM_MODULE,
	.data = __mul
};
void __mul(PExecuteState state) {
	PStackState stack = state->session->stack;
	StackPush(stack, StackPop(stack) * StackPop(stack));
}



static TWord w_sub = {
	.name = "-",
	.module = &_SYSTEM_MODULE,
	.data = __sub
};
void __sub(PExecuteState state) {
	PStackState stack = state->session->stack;
	TNumber temp = StackPop(stack);
	StackPush(stack, StackPop(stack) - temp);
}



static TWord w_div = {
	.name = "/",
	.module = &_SYSTEM_MODULE,
	.data = __div
};
void __div(PExecuteState state) {
	PStackState stack = state->session->stack;
	TNumber temp = StackPop(stack);
	StackPush(stack, StackPop(stack) / temp);
}

/////////////////////////////////////////////////

const PWord BUILDIN_WORDS[] = {
	&w_hello,
	&w_bye,
	&w_false,
	&w_true,
	&w_add,
	&w_sub,
	&w_mul,
	&w_div
};

const TNumber BUILDIN_COUNT =
    sizeof(BUILDIN_WORDS) / sizeof(PWord);
