#include <stdio.h>
#include "interpreter.h"
#include "utils/mem.h"



int main(int argc, char *argv[]) {
	char str[1024];
	int result = OK;

	MemoryInit(true);
	while (result != BYE) {
		if (result)
			printf("\nError: %d\n", result);
		else
			printf("\nOK\n");
		gets(str);
		PInterpretSession session = Interpretator(str, 0);
		result = Interpret(session);
		MemoryUseless((TAddress)session);
	}
	MemoryClean();
	return 0;
}
