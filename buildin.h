#pragma once

#include "module/mod.h"

typedef void(*TBuildinFunc)(PExecuteState state);

extern const PWord BUILDIN_WORDS[];
extern const TNumber BUILDIN_COUNT;


//
// Buildin methods
//
void __hello(PExecuteState state);
void __bye(PExecuteState state);
void __false(PExecuteState state);
void __true(PExecuteState state);
void __add(PExecuteState state);
void __sub(PExecuteState state);
void __mul(PExecuteState state);
void __div(PExecuteState state);
