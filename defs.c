#include "defs.h"
#include <stdio.h>
#include <assert.h>


//
// TAddress/TValue
//
TAddress newAddress(void *ptr) {
	assert(((TAddress)ptr & 1) == 0);
	return (TAddress)ptr & -2;
}

bool isAddress(TValue value) {
	return (value & 1) == 0;
}

TAddress toAddress(TValue value) {
	return (TAddress) (value & -2);
}

TValue fromAddress(TAddress addr) {
	return (TValue) (addr & -2);
}


//
// Number/TValue
//
bool isNumber(TValue value) {
	return value & 1 != 0;
}

TNumber toTNumber(TValue value) {
	return value >> 1;
}

UNumber toUNumber(TValue value) {
	return ((UNumber) value) >> 1;
}

TValue fromTNumber(TNumber num) {
	return (TValue)(num << 1) | 1;
}

TValue fromUNumber(UNumber num) {
	return (TValue)(num << 1) | 1;
}


//
// TValueArgs
//
void forEachArgs(TValueArgs args, bool (*func)(TValue arg)) {
	if (args && func) {
		while (*args) {
			if (func(*args++)) {
				break;
			}
		}
	}
}


//
// Extra
//
void printBits(size_t const size, void const* const ptr) {
	// Assumes little endian
	unsigned char *b = (unsigned char*) ptr;
	unsigned char byte;
	int i, j;

	for (i = size-1; i >= 0; i--) {
		for (j = 7; j >= 0; j--) {
			byte = (b[i] >> j) & 1;
			printf("%u", byte);
		}
	}
	printf(" %d\n", *((TValue *)ptr));
}
