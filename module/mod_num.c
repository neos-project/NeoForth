#include "mod.h"


//
// Module methods
//
static PExecuteState __init(PInterpretSession session, PWord word) {
	MemoryUse((TValue)word);
	if (word) {
		PExecuteState state = (PExecuteState)
		                      MemoryNew(sizeof(TExecuteState), null);
		if (state) {
			state->session = session;
			state->word = word;
			state->data = word->data;
		}
		return state;
	}
	MemoryUseless((TValue)word);
	return null;
}

static void __execute(PExecuteState state) {
	StackPush(state->session->stack, (TNumber) state->data);
}


//
// Module table
//
TModule _NUMBER_MODULE = {
	.Init = __init,
	.Execute = __execute
};
