#include "mod.h"
#include "../buildin.h"


//
// Module methods
//
static PExecuteState __init(PInterpretSession session, PWord word) {
	if (word) {
		PExecuteState state = (PExecuteState)
		                      MemoryNew(sizeof(TExecuteState), null);
		state->session = session;
		state->word = word;
		return state;
	}
	return null;
}

static void __execute(PExecuteState state) {
	TBuildinFunc func = state->word->data;
	if (func) {
		func(state);
	}
}


//
// Module table
//
TModule _SYSTEM_MODULE = {
	.Init = __init,
	.Execute = __execute
};
