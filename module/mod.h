#pragma once

#include "../interpreter.h"
#include "../voc/word.h"

typedef struct ExecuteState {
	PInterpretSession session;
	PWord word;
	void *data;
} TExecuteState, *PExecuteState;

typedef struct Module {
	PExecuteState (*Init)(PInterpretSession session, PWord word);
	void (*Execute)(PExecuteState state);
} TModule, *PModule;



PModule SystemMod(void);
extern TModule _SYSTEM_MODULE;

PModule NumberMod(void);
extern TModule _NUMBER_MODULE;
